#version 330

uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat3 NormalMatrix;

in  vec3 in_Normal;    // vertex normal coming in
in  vec3 in_Position;  // Position coming in

out vec3 ex_Normal;
out vec3 ex_EyeDir;

void main()
{
		//vec3 Normal = vec3(0,0,1);
        gl_Position = ProjectionMatrix * ModelViewMatrix * vec4(in_Position, 1.0);
        ex_Normal = NormalMatrix * in_Normal;
        ex_EyeDir = vec3(ModelViewMatrix * vec4(in_Position,1.0));
}