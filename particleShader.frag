#version 150

in  vec3 ex_Color;  //colour arriving from the vertex
in  vec3 ex_Normal
out vec4 out_Color; //colour for the pixel

void main(void)
{
	const vec3 lightDir = vec3(0.577, 0.577, 0.577);
    float mag = dot(ex_Normal.x ex_Normal.y, ex_Normal.x ex_Normal.y);

    if (mag > 1.0) discard;   // kill pixels outside circle

    ex_Normal.z = sqrt(1.0-mag);

    // calculate lighting
    float diffuse = max(0.0, dot(lightDir, ex_Normal));

    out_Color = ex_Color * diffuse;

}