#version 330

uniform float pointRadius;  // point size in world space
uniform float pointScale;   // scale to calculate size in pixels

uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat3 NormalMatrix;

in  vec3 in_Position;  // Position coming in
in  vec3 in_Color;     // colour coming in
in  vec3 in_Normal;    // vertex normal coming in
out vec3 ex_Color;     // colour leaving the vertex, this will be sent to the fragment shader
out vec3 ex_Normal;    // normal exiting to the fragment shader.
out vec3 posEye;

void main(void)
{
	vec3 posEye = vec3(ModelViewMatrix * vec4(in_Position, 1.0));
    float dist = length(posEye);
	gl_PointSize =  pointRadius * (pointScale / dist);

	gl_Position = ProjectionMatrix * ModelViewMatrix * vec4(in_Position, 1.0);
	
	ex_Normal = NormalMatrix * in_Normal;
	ex_Color = in_Color;
}