#pragma once

#include "../gl\glew.h"
#include "../glm\glm.hpp"
#include "../shaders/Shader.h"
#include "../SOIL.h"
#include <vector>
#include <iostream>

using namespace std;

class CubeMap{
private:
	//OpenGL variables
	GLuint vbo, vao;
	GLuint cubeTex;
	vector<const GLchar*> faceTex;

	//GLSL Shader
	Shader* cubeMapShader;

public:

	CubeMap(Shader* cubeMapShader);

	void createVBO();

	void createCubeMap();
	bool loadCubeMapImage(GLuint texture, GLenum side_target, const char* file_name);

	void draw();

	GLuint getCubeMap(){return this->cubeTex;}


};