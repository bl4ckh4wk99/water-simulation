#version 330

in  vec3 ex_Color;  //colour arriving from the vertex
in  vec3 ex_Normal;
in  vec3 posEye;
out vec4 out_Color; //colour for the pixel

vec4 mat_specular=vec4(1); 
vec4 light_specular=vec4(1);
const vec3 lightDir = vec3(0.577, 0.577, 0.577);
const vec4 lightAmbient = vec4(0.3, 0.3, 0.3, 1.0);

void main(void)
{
	vec2 NdotXY = gl_PointCoord* 2.0 - vec2(1.0);    
	float mag = dot(NdotXY, NdotXY);

    if (mag > 1.0) 
		discard;   // kill pixels outside circle
		   
	float diffuse = max(0.0, dot(lightDir, ex_Normal));

	vec3 halfVector = normalize( posEye + lightDir);
    float spec = max( pow(dot(ex_Normal,halfVector), 2), 0.0); 
    vec4 S = light_specular*mat_specular* spec;

    out_Color = vec4(ex_Color,1.0) * (diffuse + S + lightAmbient);
}