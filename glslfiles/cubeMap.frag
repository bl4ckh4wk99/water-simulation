#version 330 

in vec3 TexCoords;
out vec4 color;

uniform samplerCube skybox;

void main()
{    
	//colour is calculated using texture
    color = texture(skybox, TexCoords);
}
  