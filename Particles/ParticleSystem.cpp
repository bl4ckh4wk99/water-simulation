#include"ParticleSystem.h"
#include <iostream>

//deconstructor to call the cuda end function
ParticleSystem::~ParticleSystem(){

	cudaEnd();
}

//initialise cuda and all the particles
void ParticleSystem::init(int cubeDim, GLuint cubeMapTexture){

	createCudaParams(NUM_OF_PARTICLES, PARTICLE_RADIUS, GRAVITY, DAMPING, cubeDim);

	cout << "Num of particles = " <<  NUM_OF_PARTICLES << endl;
	int iterator = 0;
	int colIterator = 0;
	int size = pow(NUM_OF_PARTICLES,1/3.) + 0.5;

	//arrange in a cube formation
	for(int z=- size/2; z < size/2; z++){
		float zpos = z * (PARTICLE_RADIUS + 10);
	
		for(int y = - size/2; y < size/2; y++){
			float ypos = y * (PARTICLE_RADIUS + 10);
	
			for(int x = - size/2; x < size/2; x++){
				float xpos = x * (PARTICLE_RADIUS + 12);
				
				
				glm::vec3 pos = glm::vec3(xpos + 5,ypos,zpos);
				this->positions[iterator] = pos;
				this->velocities[iterator] = glm::vec3(0,0,0);
	
				//in pretty colours
				if(iterator < NUM_OF_PARTICLES/7){
					cols[colIterator]   = 0.5;
					cols[colIterator+1] = 0.0;
					cols[colIterator+2] = 1.0;
				}else if(iterator < 2 * (NUM_OF_PARTICLES/7)){
					cols[colIterator]   = 0.29;
					cols[colIterator+1] = 0.0;
					cols[colIterator+2] = 0.51;
				}else if(iterator < 3 * (NUM_OF_PARTICLES/7)){
					cols[colIterator]   = 0.0;
					cols[colIterator+1] = 0.0;
					cols[colIterator+2] = 1.0;
				}else if(iterator < 4 * (NUM_OF_PARTICLES/7)){
					cols[colIterator]   = 0.0;
					cols[colIterator+1] = 1.0;
					cols[colIterator+2] = 0.0;
				}else if(iterator < 5 * (NUM_OF_PARTICLES/7)){
					cols[colIterator]   = 1.0;
					cols[colIterator+1] = 1.0;
					cols[colIterator+2] = 0.0;
				}else if(iterator < 6 * (NUM_OF_PARTICLES/7)){
					cols[colIterator]   = 1.0;
					cols[colIterator+1] = 0.5;
					cols[colIterator+2] = 0.0;
				}else{
					cols[colIterator]   = 1.0;
					cols[colIterator+1] = 0.0;
					cols[colIterator+2] = 0.0;
				}
				iterator ++;
				colIterator += 3;
				if(iterator == NUM_OF_PARTICLES){
					cudaInit(positions, velocities, cubeMapTexture);
					return;
				}
			}
		}
	}

	cudaInit(positions, velocities,cubeMapTexture);
}

//update function that calls the relevant cuda ones
void ParticleSystem::update(double deltaT, Cube riverCube, Cube cliffCube, Cube plungeCube){

	cudaUpdatePos(deltaT);
	cudaCollideCube(riverCube, cliffCube, plungeCube);
	
	glm::vec3* cudaPos = cudaGetPos();
	for( int i = 0; i < NUM_OF_PARTICLES; i ++){
		this->positions[i] = cudaPos[i];
	}

}

//draw method for the point sprites
void ParticleSystem::draw(Shader* particleShader){
	int iterator = 0;
	for (int i=0; i < NUM_OF_PARTICLES; i++){
		verts[iterator] = positions[i].x;
		verts[iterator+1] = positions[i].y;
		verts[iterator+2] = positions[i].z;

		norms[iterator]   = 0.0;
		norms[iterator+1] = 0.0;
		norms[iterator+2] = -1.0;

		points[i] = i;

		iterator += 3;
	}

	// VAO allocation
	glGenVertexArrays(1, &m_vaoID);
	// First VAO setup
	glBindVertexArray(m_vaoID);
	glGenBuffers( 3, m_vboID);
	
	//set vertices buffer
	glBindBuffer( GL_ARRAY_BUFFER, m_vboID[0]); 
	glBufferData( GL_ARRAY_BUFFER, NUM_OF_PARTICLES*3*sizeof(GLfloat), verts, GL_DYNAMIC_DRAW );
	GLuint pos_location = glGetAttribLocation(particleShader->handle(), "in_Position");
	glVertexAttribPointer( pos_location, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	glEnableVertexAttribArray(pos_location );

	//set colour buffer
	glBindBuffer( GL_ARRAY_BUFFER, m_vboID[1]); 
	glBufferData( GL_ARRAY_BUFFER, NUM_OF_PARTICLES*3*sizeof(GLfloat), cols, GL_DYNAMIC_DRAW );
	GLuint colour = glGetAttribLocation(particleShader->handle(), "in_Color");
	glVertexAttribPointer( colour, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	glEnableVertexAttribArray(colour );

	//set normals buffer
	glBindBuffer( GL_ARRAY_BUFFER, m_vboID[2]); 
	glBufferData( GL_ARRAY_BUFFER, NUM_OF_PARTICLES*3*sizeof(GLfloat), norms, GL_DYNAMIC_DRAW );
	GLuint normal = glGetAttribLocation(particleShader->handle(), "in_Normal");
	glVertexAttribPointer( normal, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	glEnableVertexAttribArray(normal );

	//unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, NUM_OF_PARTICLES * sizeof(unsigned int), points, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glEnableVertexAttribArray(0);
	glBindVertexArray(0);

	//DRAW
	glBindVertexArray(m_vaoID);
	glBindBuffer(GL_ARRAY_BUFFER, ibo);
	glDrawArrays(GL_POINTS,0,NUM_OF_PARTICLES);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0); //unbind the vertex array object
}

//draw method for cuda
void ParticleSystem::drawMeta(Shader* metaShader){

	cudaDraw(metaShader);

}

