#pragma once
#include "../glm\glm.hpp"
#include "../Geometry/Sphere.h"
#include "../shaders\Shader.h"

class Particle{
private:
	glm::vec3 centre;
	glm::vec3 velocity;
public:

	Particle(){}
	Particle(glm::vec3 pos);
	~Particle();

	void update(double deltaT);

	void setCentre(glm::vec3 centre){this->centre = centre;}
	glm::vec3 getCentre(){return this->centre;}

	void setVelocity(glm::vec3 newVel){this->velocity = newVel;}
	glm::vec3 getVelocity(){return this->velocity;}


};