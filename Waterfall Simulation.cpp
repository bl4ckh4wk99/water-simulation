/**********************************************************
Waterfall Simulation

using CUDA

Chris Osman (2015/16)

Pressing 'p' will swap between marching cubes and point sprites

Pressing 'm' will toggle the models on and off

Pressing 'b' will toggle the collision and simulation boxes on and off

Rotate the camera by using the left and right arrow keys

Zoom the camera in and out using the up and down arrow keys
//********************************************************/

#include <windows.h>		// Header File For Windows
#include <math.h>
#include "console.h"        //Used for displaying the console
#include "shaders/Shader.h"   // include shader header file, this is not part of OpenGL
                              //Shader.h has been created using common glsl structure

//---------------------GLM---------------------
#include "glm\glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"
//---------------------------------------------

//------------------ OpenGL -------------------
#include "gl/glew.h"
#include "gl/wglew.h"
#pragma comment(lib, "glew32.lib")
//---------------------------------------------

//---------------- Model Loading --------------
#include "ThreeDModel\threeDModel.h"
#include "ThreeDModel\Obj\OBJLoader.h"
#include "ThreeDModel\Octree\Octree.h"
//---------------------------------------------

//-------------------Geometry------------------
#include "Geometry\Cube.h"
#include "Geometry\CubeMap.h"
Cube systemCube;
Cube riverCube;
Cube cliffCube;
Cube plungeCube;
#define SYSTEM_DIM 100

CubeMap* cubeMap;

//--------------------Camera-------------------
#include "Camera.h"
Camera cam;

//--------------------DeltaT-------------------
#include <time.h>
// Timer 
clock_t currentTime = clock();
clock_t prevTime = clock();
double deltaT = 0;
double prevDeltaT = 0;
//---------------------------------------------

//--------------------CUDA---------------------
#include <cuda_runtime.h>
//--------------------------------------------


#include <iostream>
using namespace std;

//-------------Program VARIABLES--------------
int	mouse_x=0, mouse_y=0;
bool LeftPressed = false;
int screenWidth=600, screenHeight=600;
bool keys[256];

//Shader objects
Shader myShader;
Shader particleShader;
Shader PhongShader;
Shader metaShader;
Shader modelShader;
Shader cubeMapShader;
Shader reflectShader;

ConsoleWindow console;
const double PI = 3.14159265358979323846f; 

//boolean operators
bool paused = false;
bool sprites = false;
bool models = true;
bool boxes = false;

ThreeDModel* plunge;
ThreeDModel* cliff;
ThreeDModel* river;
OBJLoader objLoader;
//------------------------------------------


//OPENGL FUNCTION PROTOTYPES
void init();							//called in winmain when the program starts.
void reshape(int width, int height);	//called when the window is resized
void update(double deltaT);				//called in winmain to update the simulation
void display();							//called in winmain to draw everything to the screen
void processKeys();						//called in winmain to process keyboard input

//Model Prototypes
ThreeDModel* loadModel(char* modelName, Shader* myShader);

/*************    START OF OPENGL FUNCTIONS   ****************/

glm::mat4 ProjectionMatrix; // matrix for the orthographic projection
glm::mat4 ModelViewMatrix;  // matrix for the modelling and viewing


//--------OpenGL VARIABLES--------
#include"Particles/ParticleSystem.h"
ParticleSystem ps;


//Model Material properties
float Mod_Material_Ambient[4] = {0.7, 0.7, 0.7, 1.0};
float Mod_Material_Diffuse[4] = {0.8, 0.8, 0.5, 1.0};
float Mod_Material_Specular[4] = {0.9,0.9,0.8,1.0};
float Mod_Material_Shininess = 50;

//Water Material properties
float Wat_Material_Ambient[4] = {0.7, 0.7, 0.7, 1.0};
float Wat_Material_Diffuse[4] = {0.8, 0.8, 0.5, 1.0};
float Wat_Material_Specular[4] = {0.9,0.9,0.8,1.0};
float Wat_Material_Shininess = 100;

//Light Properties
float Light_Ambient_And_Diffuse[4] = {0.8, 0.8, 0.6, 1.0};
float Light_Specular[4] = {1.0,1.0,1.0,1.0};
float LightPos[4] = {0.0, 200.0, 50.0, 0.0};

//--------------------------------


void init()
{
	glClearColor(0.0,0.0,0.0,0.0);

	//------------------Shaders---------------------
	if(!myShader.load("Basic", "glslfiles/basicShader.vert", "glslfiles/basicShader.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	if(!metaShader.load("Marching Cubes", "glslfiles/basicShader.vert", "glslfiles/basicShader.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	if(!PhongShader.load("Phong Shader", "glslfiles/Phong.vert", "glslfiles/Phong.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	if(!particleShader.load("Particles", "glslfiles/particleShader.vert", "glslfiles/particleShader.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	if(!modelShader.load("Model", "glslfiles/modelShader.vert", "glslfiles/modelShader.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	if(!cubeMapShader.load("CubeMap", "glslfiles/cubeMap.vert", "glslfiles/cubeMap.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	if(!reflectShader.load("Reflect", "glslfiles/Reflect.vert", "glslfiles/Reflect.frag"))
	{
		cout << "failed to load shader" << endl;
	}
	//------------------Model Loading---------------------

	river = loadModel("ThreeDModel/Models/Top2.obj",&modelShader);
	cliff = loadModel("ThreeDModel/Models/Middle2.obj",&modelShader);
	plunge = loadModel("ThreeDModel/Models/Bottom2.obj",&modelShader);


	//------------------Object Creation---------------------
	systemCube = Cube(glm::vec3(0,0,0),SYSTEM_DIM*2,SYSTEM_DIM*2,SYSTEM_DIM*2);
	systemCube.constructGeometry(&myShader);

	riverCube = Cube(glm::vec3(5,-5,20),50,20,120);
	riverCube.constructGeometry(&myShader);

	cliffCube = Cube(glm::vec3(5,-40,-20),50,90,30);
	cliffCube.constructGeometry(&myShader);

	plungeCube = Cube(glm::vec3(0,-80,-50),70,30,90);
	plungeCube.constructGeometry(&myShader);
	
	//cubeMap
	cubeMap = new CubeMap(&cubeMapShader);
	GLuint cubeTex = cubeMap->getCubeMap();

	ps = ParticleSystem();
	ps.init(SYSTEM_DIM, cubeTex);

	cam = Camera(deltaT, SYSTEM_DIM);

	
	
	glEnable(GL_DEPTH_TEST);
}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth = width; 
	screenHeight = height;

	glViewport(0,0,width,height);						// set Viewport dimensions

	ProjectionMatrix = glm::perspective(60.0f, (GLfloat)screenWidth/(GLfloat)screenHeight, 1.0f, 2000.0f);
}

void update(double deltaT){
	if(!paused){
	
		ps.update(deltaT, riverCube, cliffCube, plungeCube);
		
	}
	cam.update(deltaT);
}

void display()									
{
	//----------------------------Camera-------------------------
	glm::mat4 viewMatrix = glm::lookAt(cam.getPos(),cam.getLookat(), cam.getUp());


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glMatrixMode(GL_MODELVIEW);

	//----------------------------Cube Map-------------------------
	glDisable(GL_DEPTH_TEST);

	glDepthMask(GL_FALSE);

	glUseProgram(cubeMapShader.handle());

	GLuint matLocation = glGetUniformLocation(cubeMapShader.handle(), "ProjectionMatrix");  
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

	glm::mat4 view = glm::mat4(glm::mat3(viewMatrix)); 
	glUniformMatrix4fv(glGetUniformLocation(cubeMapShader.handle(), "ViewMatrix"), 1, GL_FALSE, &view[0][0]);

	cubeMap->draw();

	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);
	//----------------------------Models-------------------------
	if(models){
		glUseProgram(modelShader.handle());

		matLocation = glGetUniformLocation(modelShader.handle(), "ProjectionMatrix");  
		glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

		glUniformMatrix4fv(glGetUniformLocation(modelShader.handle(), "ViewMatrix"), 1, GL_FALSE, &viewMatrix[0][0]);

		glUniform4fv(glGetUniformLocation(modelShader.handle(), "LightPos"), 1, LightPos);
		glUniform4fv(glGetUniformLocation(modelShader.handle(), "light_ambient"), 1, Light_Ambient_And_Diffuse);
		glUniform4fv(glGetUniformLocation(modelShader.handle(), "light_diffuse"), 1, Light_Ambient_And_Diffuse);
		glUniform4fv(glGetUniformLocation(modelShader.handle(), "light_specular"), 1, Light_Specular);

		glUniform4fv(glGetUniformLocation(modelShader.handle(), "material_ambient"), 1, Mod_Material_Ambient);
		glUniform4fv(glGetUniformLocation(modelShader.handle(), "material_diffuse"), 1, Mod_Material_Diffuse);
		glUniform4fv(glGetUniformLocation(modelShader.handle(), "material_specular"), 1, Mod_Material_Specular);
		glUniform1f(glGetUniformLocation(modelShader.handle(), "material_shininess"), Mod_Material_Shininess);

		ModelViewMatrix = viewMatrix;

		glUniformMatrix4fv(glGetUniformLocation(modelShader.handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		glUniformMatrix3fv(glGetUniformLocation(modelShader.handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

		river->drawElementsUsingVBO(&modelShader);
		cliff->drawElementsUsingVBO(&modelShader);
		plunge->drawElementsUsingVBO(&modelShader);

		glUseProgram(0);
	}

	//----------------------------CUBE---------------------------
	if(boxes){
		glUseProgram(myShader.handle());
	
	
		matLocation = glGetUniformLocation(myShader.handle(), "ProjectionMatrix");  
		glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

		ModelViewMatrix = viewMatrix;

		glUniformMatrix4fv(glGetUniformLocation(myShader.handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

		systemCube.draw();
		if(models){
			riverCube.draw();
			cliffCube.draw();
			plungeCube.draw();
		}

		glUseProgram(0);
	}
	
	//-----------------------------PARTICLES------------------------
	//--------------------------Using MetaBalls----------------------
	if(!sprites){

		//************************PHONG SHADER*********************
		//glUseProgram(PhongShader.handle());
	
		//matLocation = glGetUniformLocation(PhongShader.handle(), "ProjectionMatrix");  
		//glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

		//glUniform4fv(glGetUniformLocation(PhongShader.handle(), "LightPos"), 1, LightPos);
		//glUniform4fv(glGetUniformLocation(PhongShader.handle(), "light_ambient"), 1, Light_Ambient_And_Diffuse);
		//glUniform4fv(glGetUniformLocation(PhongShader.handle(), "light_diffuse"), 1, Light_Ambient_And_Diffuse);
		//glUniform4fv(glGetUniformLocation(PhongShader.handle(), "light_specular"), 1, Light_Specular);

		////glUniform4fv(glGetUniformLocation(PhongShader.handle(), "material_ambient"), 1, Material_Ambient);
		////glUniform4fv(glGetUniformLocation(PhongShader.handle(), "material_diffuse"), 1, Material_Diffuse);
		////glUniform4fv(glGetUniformLocation(PhongShader.handle(), "material_specular"), 1, Material_Specular);
		//glUniform1f(glGetUniformLocation(PhongShader.handle(), "material_shininess"), Wat_Material_Shininess);

		//ModelViewMatrix = viewMatrix;

		//glUniformMatrix4fv(glGetUniformLocation(PhongShader.handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		//glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		//glUniformMatrix3fv(glGetUniformLocation(PhongShader.handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

		//ps.drawMeta(&PhongShader);


		//************************REFLECT SHADER*********************
		

		glUseProgram(reflectShader.handle());

		matLocation = glGetUniformLocation(reflectShader.handle(), "ProjectionMatrix");  
		glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

		ModelViewMatrix = viewMatrix;

		glUniformMatrix4fv(glGetUniformLocation(reflectShader.handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
		glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		glUniformMatrix3fv(glGetUniformLocation(reflectShader.handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

		ps.drawMeta(&reflectShader);

		glUseProgram(0);
	}

	//--------------------------Using GLPoints----------------------
	else{
		glEnable(GL_POINT_SPRITE_ARB);
		glTexEnvi(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);
		glEnable(GL_VERTEX_PROGRAM_POINT_SIZE_NV);
		glDepthMask(GL_TRUE);
		glEnable(GL_DEPTH_TEST);

		glUseProgram(particleShader.handle());
		glUniform1f(glGetUniformLocation(particleShader.handle(), "pointScale"), screenHeight / tanf(60.0f*0.5f*(float)PI/180.0f));
		glUniform1f(glGetUniformLocation(particleShader.handle(), "pointRadius"), PARTICLE_RADIUS);

		matLocation = glGetUniformLocation(particleShader.handle(), "ProjectionMatrix");  
		glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

		ModelViewMatrix = glm::translate(viewMatrix,glm::vec3(0,0,0)); 	
		glUniformMatrix4fv(glGetUniformLocation(particleShader.handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
	
		glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
		glUniformMatrix3fv(glGetUniformLocation(particleShader.handle(), "NormalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));

		ps.draw(&particleShader);
		glUseProgram(0);
	}
	
	glFlush();
}


void processKeys(){
	if(keys[VK_UP]){
		float deltaZoom = 200.0f * deltaT;
		cam.zoomIn(deltaZoom);
	}
	if(keys[VK_DOWN]){
		float deltaZoom = 200.0f * deltaT;
		cam.zoomOut(deltaZoom);
	}
	if(keys[VK_LEFT]){
		float deltaAngle = 100.0f * deltaT;
		cam.panLeft(deltaAngle);
	}
	if(keys[VK_RIGHT]){
		float deltaAngle = 100.0f * deltaT;
		cam.panRight(deltaAngle);
	}
	if(keys[VK_SPACE]){
		paused = !paused;
	}
	if(keys['R']){
		GLuint cubeTex = cubeMap->getCubeMap();
		ps.init(SYSTEM_DIM, cubeTex);
	}
	if(keys['P']){
		sprites = !sprites;
		keys['P'] = false;
	}
	if(keys['M']){
		models = !models;
		keys['M'] = false;
	}
	if(keys['B']){
		boxes = !boxes;
		keys['B'] = false;
	}
	
}

ThreeDModel* loadModel(char* modelName, Shader* myShader){

	ThreeDModel *model = new ThreeDModel();

	cout << " loading Object " << endl;
	if(objLoader.loadModel(modelName, *model))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " Object loaded " << endl;
		model->calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.
		//turn on VBO by setting useVBO to true in threeDmodel.cpp default constructor - only permitted on 8 series cards and higher
		model->initDrawElements();
		model->initVBO(myShader);
		model->deleteVertexFaceData();
		return model;
	}
	else
	{
		cout << " Object failed to load " << endl;
	}

}



/**************** END OPENGL FUNCTIONS *************************/

//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	console.Open();
	
	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop

	// Create Our OpenGL Window
	if (!CreateGLWindow("Waterfall Simulation",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			if(keys[VK_ESCAPE])
				done = true;

			processKeys();			//process keyboard
			
			//Timer
			//get current time
			currentTime = clock();
			clock_t clockTicksTaken = currentTime - prevTime;
			deltaT = (clockTicksTaken / (double)CLOCKS_PER_SEC);					

			//// Advance timer
			prevTime = currentTime;					// use the current time as the previous time in the next step
			prevDeltaT = deltaT;					// use the current deltaT as the previous deltaT in the next step

			update(deltaT);					// update variables
			display();					// Draw The Scene
			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	console.Close();

	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		break;
		case WM_LBUTTONDOWN:
		{
	        mouse_x = LOWORD(lParam);          
			mouse_y = screenHeight - HIWORD(lParam);
			LeftPressed = true;
		}
		break;

		case WM_LBUTTONUP:
			{
	            LeftPressed = false;
			}
		break;
		case WM_MOUSEWHEEL:
			{
				short wheelDir = GET_WHEEL_DELTA_WPARAM(wParam);
				if(wheelDir > 0){
					float deltaZoom = 10000.0f * deltaT;
					cam.zoomIn(deltaZoom);
				}
				if(wheelDir < 0){
					float deltaZoom = 10000.0f * deltaT;
					cam.zoomOut(deltaZoom);
				}
			}

		break;
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
 
bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	HGLRC tempContext = wglCreateContext(hDC);
	wglMakeCurrent(hDC, tempContext);
	
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		cout << " GLEW ERROR" << endl;
		
	}
	
	int attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 2,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0
	};
	
    if(wglewIsSupported("WGL_ARB_create_context") == 1)
    {
		hRC = wglCreateContextAttribsARB(hDC,0, attribs);
		wglMakeCurrent(NULL,NULL);
		wglDeleteContext(tempContext);
		wglMakeCurrent(hDC, hRC);
	}
	else
	{	//It's not possible to make a GL 3.x context. Use the old style context (GL 2.1 and before)
		hRC = tempContext;
		cout << " not possible to make context "<< endl;
	}

	//Checking GL version
	const GLubyte *GLVersionString = glGetString(GL_VERSION);

	cout << "OpenGL version: " << GLVersionString << endl;

	//We can check the version in OpenGL 
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	cout << "OpenGL Version: " << OpenGLVersion[0] << " " << OpenGLVersion[1] << endl;

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	
	init();

	reshape(width, height);     					// Set Up Our Perspective GL Screen
	
	
	return true;									// Success
}
