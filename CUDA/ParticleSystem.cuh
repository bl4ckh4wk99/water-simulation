#pragma once
#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#define CUDA_VERSION   7500
#define CUDA_ARCH sm_35
#define GLM_FORCE_CUDA 

//Kernel Constants
#define CUDART_PI_F 3.141592654f
#define CUDART_K_F 0.4615f

// Helper functions and utilities to work with CUDA
#include <helper_functions.h>
#include <helper_cuda.h> 
#include <helper_math.h>
#include <cuda_runtime_api.h>

//program includes
#include "../glm\glm.hpp"
#include "../shaders\Shader.h"
#include "../Geometry/Cube.h"

//****************LIST OF FUNCTIONS AND KERNELS****************
//****************1....... PARAMS..............****************
//****************2....... GET METHODS.........****************
//****************3....... CREATE PARAMS.......****************
//****************4....... CUDA INIT...........****************
//****************5....... KERNELS.............****************
//****************6....... UPDATE POS..........****************
//****************7....... DRAW ...............****************
//****************8....... CUBE COLLISION......****************
//****************9...... MARCHING CUBES......****************

//****************1....... PARAMS..............****************
struct params{
	//SIM PARAMETERS
	glm::vec3 gravity;
	int numOfParticles;
	float particleRadius;
	//float spring;
	float damping;
	glm::vec3* velocities;
	glm::vec3* positions;
	float* densities;
	glm::vec3* forces;
	glm::vec3* d_velocities;
	glm::vec3* d_positions;
	float* d_densities;
	glm::vec3* d_forces;
	int cubeDim;

	//Marching Cubes
	glm::vec3* triangles;
	glm::vec3* d_triangles;
	glm::vec3* normals;
	glm::vec3* d_normals;
	int nTriangles;
	int* d_nTriangles;
	

	//SPACE PARTION PARAMETERS
	int gridSize;
	int numOfCells;
	int cellSize;
	int partsPerCell;
	int arraySize;
	int* grid;
	int* d_grid;
	int* neighbours;
	int* d_neighbours;
	int k;

	//Marching Cubes
	int* d_edgeTable;
	int* d_triTable;
	int numOfCubes;
	int cubeSize;
	int cellsPerRow;
	int cellsPerSlice;
	glm::vec3* cubeVerts;
	float* cubeVals;
	glm::vec3* tempVerts;
	float* tempVals;

	//VBO
	//metaballs
	float* metaVerts;
	float* metaCols;
	float* metaNorms;
	unsigned int* tris;
	unsigned int vaoID;		    // vertex array object
	unsigned int vboID[3];		// three VBOs - used for colours, vertex and normals data
	GLuint metaIBO;             // identifier for the point indices

	unsigned int vaoG;
	unsigned int vboIDG;
	float* cVertsF;

	//variables
	float gooFacter;
	GLuint cubeMapTex;
}Basic;

//****************2....... GET METHODS.........****************
glm::vec3* cudaGetPos();

glm::vec3* getTriangles();

glm::vec3* getNormals();

int getNumOfTriangles();

int getNumOfCells();


//****************3....... CREATE PARAMS.......****************
void createCudaParams(int numOfParticles, int particleRad, float gravity, float damping, int cubeDim);


//****************4....... CUDA INIT & DECON.....****************
void cudaInit(glm::vec3* positions, glm::vec3* velocities, GLuint cubeMapTexture);

void cudaEnd();

__global__ void calcCubeVerts(glm::vec3* cubeVerts, int gridSize, int cubeSize, int numOfCubes, glm::vec3 startPos, int cellsPerRow, int cellsPerSlice);

void createCubeVerts();


//****************5....... KERNELS.............****************
__device__ float W_poly6(glm::vec3 r);

__device__ glm::vec3 grad_W_spiky(glm::vec3 r);

__device__ glm::vec3 lap_W_viscosity(glm::vec3 r);

//****************6....... UPDATE POS..........****************
__global__ void updatePos(glm::vec3* positions, glm::vec3* velocities, double deltaT,
							int* d_grid,  float particleRad, int gridSize);

__global__ void calcDensities(float* densities, glm::vec3* positions, int* d_grid,  float particleRad, int gridSize, int numOfCells);

__global__ void calcForce(glm::vec3* forces, glm::vec3* positions, glm::vec3* velocities, float* densities, int* d_grid,  float particleRad, int gridSize, int numOfCells);

__global__ void calcNewVel(glm::vec3* velocities, float* densities, glm::vec3* forces, double deltaT, glm::vec3 gravity);

void cudaUpdatePos(double deltaT);


//****************7..........Draw ..............***************
void cudaDraw(Shader* myShader);


//****************8....... CUBE COLLISION......****************
__global__ void collideCube(glm::vec3* positions, glm::vec3* velocities, glm::vec3 cubeCentre, glm::vec3 cubeDim, int particleRad, float Damping, int* d_grid, int gridSize);

void cudaCollideCube(glm::vec3 cubeDimensions, glm::vec3 cubeCentre);

//****************9... MARCHING CUBES........****************
__device__ glm::vec3 VertexInterp(float isolevel,glm::vec3 p1,glm::vec3 p2,float valp1,float valp2);

__global__ void voxelCalculation(glm::vec3* cubeVerts, float* cubeVals, glm::vec3* positions, float particleRad, int* d_grid, int d_gridSize, int numOfCells, int cellSize, int partsPerCell);

__global__ void generateTriangles( glm::vec3* triangles, int* nTriangles, glm::vec3* cubeVerts, float* cubeVals, int* d_edgeTable, int* d_triTable);

void vertexNormals(glm::vec3* normals, glm::vec3* triangles, glm::vec3* positions, float particleRad, int* d_grid, int d_gridSize, int numOfCells, int cellSize, int partsPerCell);

void cudaMarching(Shader* myShader);

#endif
