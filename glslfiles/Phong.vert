#version 330

uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat3 NormalMatrix;
uniform vec4 LightPos;  // light position

in  vec3 in_Position;  // Position coming in
in  vec3 in_Color;     // colour coming in
in  vec3 in_Normal;    // vertex normal coming in

out vec3 ex_Color;     // colour leaving the vertex, this will be sent to the fragment shader
out vec3 N;
out vec3 v;
out vec3 ex_LightPos;

void main(void)  
{     
	v = vec3(ModelViewMatrix * vec4(in_Position, 1.0));       
	N = normalize(NormalMatrix * in_Normal);

	gl_Position = ProjectionMatrix * ModelViewMatrix * vec4(in_Position, 1.0);
	ex_Color = in_Color;
	ex_LightPos = vec3(LightPos);
}
          
          