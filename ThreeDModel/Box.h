//Adapted from Project provided by Dr Stephen Laycock for Graphics 2

#ifndef _BOX_H
#define _BOX_H

#include "gl\glew.h"

class Shader;

const int NumberOfBoxVertexCoords = 24;
const int NumberOfBoxTriangleIndices = 36;


class Box
{
private:
	float dim;
	unsigned int m_vaoID;		    // vertex array object
	unsigned int m_vboID[2];		// two VBOs - used for colours and vertex data
	GLuint ibo;                     //identifier for the triangle indices

	static int numOfVerts;
	static int numOfTris;

	float verts[NumberOfBoxVertexCoords];
	float cols[NumberOfBoxVertexCoords];
	unsigned int tris[NumberOfBoxTriangleIndices];
public:
	Box();
	void constructGeometry(Shader* myShader, float minx, float miny, float minz, float maxx, float maxy, float maxz);
	void render();
};

#endif _BOX_H