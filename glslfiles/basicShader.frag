#version 330

in  vec3 ex_Color;  //colour arriving from the vertex
out vec4 out_Color; //colour for the pixel

void main(void)
{
	//out colour is simply in colour
	out_Color = vec4(ex_Color,1.0);
}