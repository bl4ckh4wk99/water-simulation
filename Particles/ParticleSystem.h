#pragma once
#include <vector>
#include "../glm\glm.hpp"
#include "../Geometry/Cube.h"
#include "../shaders\Shader.h"
using namespace std;

#define NUM_OF_PARTICLES 125 //only works as a cube number: 216 or 125
#define PARTICLE_RADIUS 4
#define GRAVITY 10
#define DAMPING 0.7f
#define MAX_NUM_POINTS (pow((Basic.gridSize / PARTICLE_RADIUS), 3) * 5) * 3

//CUDA FUNCTIONS
#include <cuda_runtime.h>

extern void cudaInit(glm::vec3* positions, glm::vec3* velocities, GLuint cubeMapTexture);
extern void cudaEnd();
extern void createCudaParams(int numOfParticles, int particleRad, float gravity, float damping, int cubeDim);
extern glm::vec3* cudaGetPos();
extern void cudaUpdatePos(double deltaT);
extern glm::vec3* getTriangles();
extern int getNumOfTriangles();
extern void cudaMarching();
extern int getNumOfCells();
extern void cudaCollideCube(Cube riverCube, Cube cliffCube, Cube plungeCube);
extern void cudaDraw(Shader* myShader);

class ParticleSystem{
private:
	glm::vec3 positions[NUM_OF_PARTICLES];
	glm::vec3 velocities[NUM_OF_PARTICLES];

	unsigned int m_vaoID;		    // vertex array object
	unsigned int m_vboID[3];		// three VBOs - used for colours, vertex and normals data
	GLuint ibo;                     // identifier for the point indices

	float verts[NUM_OF_PARTICLES*3];
	float cols[NUM_OF_PARTICLES*3];
	float norms[NUM_OF_PARTICLES*3];
	float points[NUM_OF_PARTICLES];
public:
	ParticleSystem(){}
	~ParticleSystem();

	void init(int cubeDim, GLuint cubeMapTexture);

	void update(double deltaT, Cube riverCube, Cube cliffCube, Cube plungeCube);
	void draw(Shader* particleShader);
	void drawMeta(Shader* metaShader);

};