#version 330

in vec3 ex_Color;  //colour arriving from the vertex
in vec3 N;
in vec3 v; 
in vec3 ex_LightPos;

uniform vec4 light_ambient;
uniform vec4 light_diffuse;
uniform vec4 light_specular;

uniform float material_shininess;

out vec4 out_Color; //colour for the pixel

void main (void)  
{  
   vec3 L = normalize(ex_LightPos - v);   
   vec3 E = normalize(-v); // we are in Eye Coordinates, so EyePos is (0,0,0)  
   vec3 R = normalize(-reflect(L,N));  
 
   //calculate Ambient Term:  
   vec4 Iamb = light_ambient;    

   //calculate Diffuse Term:  
   vec4 Idiff = light_diffuse * max(dot(N,L), 0.0);
   Idiff = clamp(Idiff, 0.0, 1.0);     
   
   // calculate Specular Term:
   vec4 Ispec = light_specular * pow( max( dot(R,E) ,0.0) ,0.3 * material_shininess);
   Ispec = clamp(Ispec, 0.0, 1.0); 

   // write Total Color:  
   out_Color = vec4(ex_Color,1.0); + Iamb + Idiff + Ispec;
}