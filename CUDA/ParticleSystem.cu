#include "../CUDA/ParticleSystem.cuh"

#include <stdio.h>

//LookUp Tables
int edgeTable [256]={	0x0  , 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
									0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
									0x190, 0x99 , 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
									0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
									0x230, 0x339, 0x33 , 0x13a, 0x636, 0x73f, 0x435, 0x53c,
									0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
									0x3a0, 0x2a9, 0x1a3, 0xaa , 0x7a6, 0x6af, 0x5a5, 0x4ac,
									0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
									0x460, 0x569, 0x663, 0x76a, 0x66 , 0x16f, 0x265, 0x36c,
									0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
									0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff , 0x3f5, 0x2fc,
									0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
									0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55 , 0x15c,
									0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
									0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc ,
									0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
									0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
									0xcc , 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
									0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
									0x15c, 0x55 , 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
									0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
									0x2fc, 0x3f5, 0xff , 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
									0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
									0x36c, 0x265, 0x16f, 0x66 , 0x76a, 0x663, 0x569, 0x460,
									0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
									0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa , 0x1a3, 0x2a9, 0x3a0,
									0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
									0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33 , 0x339, 0x230,
									0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
									0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99 , 0x190,
									0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
									0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0   };
int triTable [256*16] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1,
										3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1,
										3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1,
										3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1,
										9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1,
										9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1,
										2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1,
										8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1,
										9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1,
										4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1,
										3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1,
										1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1,
										4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1,
										4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1,
										9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1,
										5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1,
										2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1,
										9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1,
										0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1,
										2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1,
										10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1,
										4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1,
										5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1,
										5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1,
										9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1,
										0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1,
										1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1,
										10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1,
										8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1,
										2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1,
										7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1,
										9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1,
										2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1,
										11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1,
										9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1,
										5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1,
										11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1,
										11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1,
										1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1,
										9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1,
										5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1,
										2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1,
										0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1,
										5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1,
										6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1,
										3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1,
										6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1,
										5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1,
										1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1,
										10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1,
										6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1,
										8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1,
										7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1,
										3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1,
										5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1,
										0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1,
										9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1,
										8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1,
										5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1,
										0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1,
										6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1,
										10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1,
										10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1,
										8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1,
										1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1,
										3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1,
										0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1,
										10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1,
										3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1,
										6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1,
										9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1,
										8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1,
										3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1,
										6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1,
										0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1,
										10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1,
										10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1,
										2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1,
										7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1,
										7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1,
										2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1,
										1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1,
										11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1,
										8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1,
										0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1,
										7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1,
										10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1,
										2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1,
										6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1,
										7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1,
										2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1,
										1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1,
										10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1,
										10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1,
										0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1,
										7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1,
										6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1,
										8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1,
										9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1,
										6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1,
										4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1,
										10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1,
										8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1,
										0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1,
										1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1,
										8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1,
										10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1,
										4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1,
										10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1,
										5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1,
										11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1,
										9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1,
										6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1,
										7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1,
										3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1,
										7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1,
										9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1,
										3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1,
										6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1,
										9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1,
										1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1,
										4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1,
										7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1,
										6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1,
										3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1,
										0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1,
										6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1,
										0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1,
										11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1,
										6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1,
										5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1,
										9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1,
										1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1,
										1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1,
										10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1,
										0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1,
										5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1,
										10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1,
										11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1,
										9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1,
										7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1,
										2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1,
										8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1,
										9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1,
										9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1,
										1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1,
										9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1,
										9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1,
										5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1,
										0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1,
										10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1,
										2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1,
										0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1,
										0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1,
										9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1,
										5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1,
										3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1,
										5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1,
										8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1,
										0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1,
										9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1,
										0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1,
										1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1,
										3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1,
										4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1,
										9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1,
										11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1,
										11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1,
										2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1,
										9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1,
										3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1,
										1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1,
										4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1,
										4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1,
										0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1,
										3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1,
										3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1,
										0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1,
										9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1,
										1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
										-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

//****************LIST OF FUNCTIONS AND KERNELS****************
//****************1....... GET METHODS.........****************
//****************2....... CREATE PARAMS.......****************
//****************3....... CUDA INIT...........****************
//****************4....... KERNELS.............****************
//****************5....... UPDATE POS..........****************
//****************6....... DRAW ...............****************
//****************7....... CUBE COLLISION......****************
//****************8...... MARCHING CUBES......****************

//****************1....... GET METHODS.........****************
glm::vec3* cudaGetPos(){
	int size = sizeof(glm::vec3) * Basic.numOfParticles;
	cudaMemcpy(Basic.positions, Basic.d_positions, size, cudaMemcpyDeviceToHost);
	return Basic.positions;
}

glm::vec3* getTriangles(){
	int size = (sizeof(glm::vec3) * Basic.numOfCubes * 5);
	cudaMemcpy(Basic.triangles, Basic.d_triangles, size, cudaMemcpyDeviceToHost);
	return Basic.triangles;
}

glm::vec3* getNormals(){
	int size = (sizeof(glm::vec3) * Basic.numOfCubes * 5);
	cudaMemcpy(Basic.normals, Basic.d_normals, size, cudaMemcpyDeviceToHost);
	return Basic.normals;
}

int getNumOfTriangles(){
	int size = sizeof(int);
	cudaMemcpy(&Basic.nTriangles, Basic.d_nTriangles, size, cudaMemcpyDeviceToHost);
	return Basic.nTriangles;
}

int getNumOfCells(){return Basic.numOfCells;}


//****************2....... CREATE PARAMS.......****************

void createCudaParams(int numOfParticles, int particleRad, float gravity, float damping, int cubeDim){
	Basic.gravity = glm::vec3(0,-gravity,0);
	Basic.particleRadius = particleRad;
	Basic.numOfParticles = numOfParticles;
	Basic.damping = damping;
	Basic.velocities = new glm::vec3[Basic.numOfParticles];
	Basic.positions = new glm::vec3[Basic.numOfParticles];
	Basic.densities = new float[Basic.numOfParticles];
	Basic.forces = new glm::vec3[Basic.numOfParticles];
	Basic.cubeDim = cubeDim;

	//Space Partioning
	Basic.cellSize = 15;
	Basic.gridSize = (cubeDim * 2);
	Basic.numOfCells = pow((Basic.gridSize / Basic.cellSize), 3);
	Basic.partsPerCell = 100;
	Basic.arraySize = Basic.numOfCells * Basic.partsPerCell;
	Basic.grid = new int[Basic.arraySize];

	//Marching Cubes
	int maxNumOfTris = (Basic.numOfCells * 5) * 3;
	Basic.triangles = new glm::vec3[maxNumOfTris];
	Basic.normals = new glm::vec3[maxNumOfTris];
	Basic.nTriangles = 0;

	Basic.cubeSize = 10;
	Basic.numOfCubes = pow((Basic.gridSize / Basic.cubeSize), 3);
	Basic.cellsPerRow = ceil((double)Basic.gridSize / (double)Basic.cubeSize);
	Basic.cellsPerSlice = (Basic.cellsPerRow * Basic.cellsPerRow);

	Basic.cubeVerts = new glm::vec3[Basic.numOfCells * 8];
	Basic.cubeVals = new float[Basic.numOfCells * 8];

	//VBO
	Basic.metaVerts = new float[maxNumOfTris * 3];
	Basic.metaCols = new float[maxNumOfTris * 3];
	Basic.metaNorms = new float[maxNumOfTris * 3];
	Basic.tris = new unsigned int[maxNumOfTris * 3];

	//variables
	Basic.gooFacter = 1.5;
}


//****************3....... CUDA INIT & DECON.....****************

//create and allocate all the memory needed for the simulation
void cudaInit(glm::vec3* positions, glm::vec3* velocities, GLuint cubeMapTexture){

	for(int i = 0; i < Basic.arraySize; i ++){
		Basic.grid[i] = -1;
	}
	int Y = Basic.gridSize/Basic.particleRadius;
	int Z = Y * (Basic.gridSize/Basic.particleRadius);

	for(int i = 0; i < Basic.numOfParticles; i ++){
		Basic.positions[i].x = positions[i].x;
		Basic.velocities[i].x = velocities[i].x;
		Basic.positions[i].y = positions[i].y;
		Basic.velocities[i].y = velocities[i].y;
		Basic.positions[i].z = positions[i].z;
		Basic.velocities[i].z = velocities[i].z;

		Basic.forces[i] = glm::vec3(0,0,0);
		Basic.densities[i] = 0;

		int indexX = (positions[i].x + Basic.gridSize/2) / Basic.particleRadius;
		int indexY = (positions[i].y + Basic.gridSize/2) / Basic.particleRadius;
		int indexZ = (positions[i].z + Basic.gridSize/2) / Basic.particleRadius;
		int gridPos = indexX + (indexY * Y) + (indexZ * Z);

		if(Basic.grid[gridPos] == -1)
			Basic.grid[gridPos] = i;
		else{
			bool inserted = false;
			int j = 1;
			while(!inserted && j < Basic.partsPerCell){
				if(Basic.grid[gridPos + j] == -1){
					Basic.grid[gridPos + j] = i;
					inserted = true;
				}else{
					j ++;
				}
			}
		}
	}

//Memory Allocation
	int particleSize = sizeof(glm::vec3) * Basic.numOfParticles;
	cudaMalloc((void **)&Basic.d_positions, particleSize);
	cudaMalloc((void **)&Basic.d_velocities, particleSize);
	cudaMalloc((void **)&Basic.d_forces, particleSize);
	cudaMemcpy(Basic.d_positions, Basic.positions, particleSize, cudaMemcpyHostToDevice);
	cudaMemcpy(Basic.d_velocities, Basic.velocities, particleSize, cudaMemcpyHostToDevice);
	cudaMemcpy(Basic.d_forces, Basic.forces, particleSize, cudaMemcpyHostToDevice);	
	

	particleSize = sizeof(float) * Basic.numOfParticles;
	cudaMalloc((void **)&Basic.d_densities, particleSize);
	cudaMemcpy(Basic.d_densities, Basic.densities, particleSize, cudaMemcpyHostToDevice);


	int neighbourSize = sizeof(int) * Basic.numOfParticles * Basic.k;
	cudaMalloc((void **)&Basic.d_neighbours, neighbourSize);

	int gridSize = sizeof(int) * Basic.arraySize;
	cudaMalloc((void **)&Basic.d_grid, gridSize);
	cudaMemcpy(Basic.d_grid, Basic.grid, gridSize, cudaMemcpyHostToDevice);


	int trianglesSize = (sizeof(glm::vec3) * Basic.numOfCubes * 5);
	cudaMalloc((void **)&Basic.d_triangles, trianglesSize);
	cudaMalloc((void **)&Basic.d_normals, trianglesSize);
	cudaMalloc((void **)&Basic.d_nTriangles, sizeof(int));
	cudaMemcpy(Basic.d_triangles, Basic.triangles, trianglesSize, cudaMemcpyHostToDevice);
	cudaMemcpy(Basic.d_normals, Basic.normals, trianglesSize, cudaMemcpyHostToDevice);
	cudaMemcpy(Basic.d_nTriangles, &Basic.nTriangles, sizeof(int), cudaMemcpyHostToDevice);


	cudaMalloc((void **)&Basic.cubeVerts, (sizeof(glm::vec3) * Basic.numOfCubes * 8));
	cudaMalloc((void **)&Basic.cubeVals, (sizeof(float) * Basic.numOfCubes * 8));

	cudaMalloc((void **)&Basic.d_edgeTable, 256 * sizeof(int));
	cudaMemcpy(Basic.d_edgeTable, edgeTable, 256 * sizeof(int), cudaMemcpyHostToDevice);

	cudaMalloc((void **)&Basic.d_triTable, 256 * 16 * sizeof(int));
	cudaMemcpy(Basic.d_triTable, triTable, 256 * 16 * sizeof(int), cudaMemcpyHostToDevice);

	//create marching cube verticies
	createCubeVerts();

	//assign cube map texture
	Basic.cubeMapTex = cubeMapTexture;
}

//delete all the cuda memory allocated
void cudaEnd(){

	cudaFree(Basic.d_positions);
	cudaFree(Basic.d_velocities);
	cudaFree(Basic.d_densities);
	cudaFree(Basic.d_forces);

	cudaFree(Basic.d_grid);
	cudaFree(Basic.d_neighbours);

	cudaFree(Basic.d_triangles);
	cudaFree(Basic.d_normals);
	cudaFree(Basic.d_nTriangles);

	cudaFree(Basic.cubeVerts);
	cudaFree(Basic.cubeVals);

	cudaFree(Basic.d_edgeTable);
	cudaFree(Basic.d_triTable);
}

//create the marching cube vertex array for the marching cubes algorithm
__global__ void calcCubeVerts(glm::vec3* cubeVerts, int gridSize, int cubeSize, int numOfCubes, glm::vec3 startPos, int cellsPerRow, int cellsPerSlice){

	//Indexing
	int x = threadIdx.x;
	int y = threadIdx.y;
	int z = blockIdx.x;

	int index8 = (threadIdx.x + (threadIdx.y * cellsPerRow) + (blockIdx.x * cellsPerSlice)) * 8;

	//Calculate verticies of the cube
	//BLB = bottom left back of cube .... etc
	cubeVerts[index8    ] = glm::vec3(startPos.x + (x * cubeSize),			 startPos.y + (y * cubeSize),			 startPos.z + (z * cubeSize));				//BLB
	cubeVerts[index8 + 1] = glm::vec3(startPos.x + (x * cubeSize) + cubeSize, startPos.y + (y * cubeSize),			 startPos.z + (z * cubeSize));				//BRB
	cubeVerts[index8 + 2] = glm::vec3(startPos.x + (x * cubeSize) + cubeSize, startPos.y + (y * cubeSize),			 startPos.z + (z * cubeSize) + cubeSize);	//BRF
	cubeVerts[index8 + 3] = glm::vec3(startPos.x + (x * cubeSize),			 startPos.y + (y * cubeSize),			 startPos.z + (z * cubeSize) + cubeSize);	//BLF
	cubeVerts[index8 + 4] = glm::vec3(startPos.x + (x * cubeSize),			 startPos.y + (y * cubeSize) + cubeSize, startPos.z + (z * cubeSize));				//TLB
	cubeVerts[index8 + 5] = glm::vec3(startPos.x + (x * cubeSize) + cubeSize, startPos.y + (y * cubeSize) + cubeSize, startPos.z + (z * cubeSize));				//TRB
	cubeVerts[index8 + 6] = glm::vec3(startPos.x + (x * cubeSize) + cubeSize, startPos.y + (y * cubeSize) + cubeSize, startPos.z + (z * cubeSize) + cubeSize);	//TRF
	cubeVerts[index8 + 7] = glm::vec3(startPos.x + (x * cubeSize),			 startPos.y + (y * cubeSize) + cubeSize, startPos.z + (z * cubeSize) + cubeSize);	//TLF

}

//function to call the kernel
void createCubeVerts(){

	glm::vec3 startPos = glm::vec3(-Basic.gridSize/2, -Basic.gridSize/2, -Basic.gridSize/2);

	dim3 grid(Basic.cellsPerRow);					// grid = cellsPerRow blocks
	dim3 block(Basic.cellsPerRow,Basic.cellsPerRow);	// block = cellsPerRow x cellsPerRow threads

	calcCubeVerts<<<grid,block>>>(Basic.cubeVerts, Basic.gridSize, Basic.cubeSize, Basic.numOfCubes, startPos, Basic.cellsPerRow, Basic.cellsPerSlice);

}

//****************4....... Kernels.............****************
//density kernel
__device__ float W_poly6(glm::vec3 r){

	float h = 3.0;
	glm::vec3 r2 = glm::abs(r);

	float ans = ( 315 / (64 * CUDART_PI_F * pow(h,9)) ) * pow(pow(h,2) - glm::dot(r2,r2),3);
	return ans;
}
//pressure kernel
__device__ glm::vec3 grad_W_spiky(glm::vec3 r){
	float h = 3.0;
	float R = glm::length(r);
	glm::vec3 ans = -r * ((float)45 / (CUDART_PI_F * pow(h,6) /* * R */)) * pow((h-R),2);
	return ans;
}
//viscosity kernel
__device__ glm::vec3 lap_W_viscosity(glm::vec3 r){
	float h = 3.0;
	glm::vec3 ans = 45 / (CUDART_PI_F * pow(h,5)) * ((float)1 - (r/h));
	return ans;
}


//****************5....... UPDATE POS..........****************
//update the particles positions
__global__ void updatePos(glm::vec3* positions, glm::vec3* velocities, double deltaT,
							int* d_grid,  float particleRad, int gridSize, int cellSize, int partsPerCell, glm::vec3 gravity) {

	//**********calc new positions*********
	positions[threadIdx.x] += (velocities[threadIdx.x] * (float)deltaT);

	//***********set new grid pos**********
	int Y = gridSize/cellSize;
	int Z = Y * (gridSize/cellSize);

	int indexX = (positions[threadIdx.x].x + gridSize/2) / cellSize;
	int indexY = (positions[threadIdx.x].y + gridSize/2) / cellSize;
	int indexZ = (positions[threadIdx.x].z + gridSize/2) / cellSize;
	int gridPos = indexX + (indexY * Y) + (indexZ * Z);

	if(d_grid[gridPos] == -1)
		d_grid[gridPos] = threadIdx.x;
	else{
		bool inserted = false;
		int i = 1;
		while(!inserted && i < partsPerCell){
			if(d_grid[gridPos + i] == -1){
				d_grid[gridPos + i] = threadIdx.x;
				inserted = true;
			}else{
				i ++;
			}
		}
	}

	
}
//kernel to calculate each particles density
__global__ void calcDensities(float* densities, glm::vec3* positions, int* d_grid,  float particleRad, int d_gridSize, int cellSize, int numOfCells, int partsPerCell){
	float density = 0;

	int Y = d_gridSize/cellSize;
	int Z = Y * (d_gridSize/cellSize);

	int indexX = (positions[threadIdx.x].x + d_gridSize/2) / cellSize;
	int indexY = (positions[threadIdx.x].y + d_gridSize/2) / cellSize;
	int indexZ = (positions[threadIdx.x].z + d_gridSize/2) / cellSize;
	int gridPos = indexX + (indexY * Y) + (indexZ * Z);

	for(int x = -2; x <= 2; x++){
		for(int y = -2; y<= 2; y++){
			for(int z = -2; z <= 2; z++){
				int cubeIndex = gridPos-x + y*Y - z*Z;
				if(cubeIndex > 0 && cubeIndex < numOfCells){
					for(int i=0; d_grid[cubeIndex+i]!= -1 && i < partsPerCell; i++){
						//perform density calculation
						glm::vec3 r = positions[threadIdx.x] - positions[d_grid[cubeIndex]];
						float temp =  W_poly6(r);
						density  = density + (particleRad * temp);
					}
				}
			}
		}
	}
	densities[threadIdx.x] = density;
}
//kernel to calculate each particles pressure and viscosity
__global__ void calcForce(glm::vec3* forces, glm::vec3* positions, glm::vec3* velocities, float* densities, int* d_grid,  float particleRad, int d_gridSize, int cellSize, int numOfCells, int partsPerCell){

	glm::vec3 force = glm::vec3(0,0,0);
	float pressure_p = CUDART_K_F * densities[threadIdx.x];

	//get grid pos
	int Y = d_gridSize/cellSize;
	int Z = Y * (d_gridSize/cellSize);

	int indexX = (positions[threadIdx.x].x + d_gridSize/2) / cellSize;
	int indexY = (positions[threadIdx.x].y + d_gridSize/2) / cellSize;
	int indexZ = (positions[threadIdx.x].z + d_gridSize/2) / cellSize;
	int gridPos = indexX + (indexY * Y) + (indexZ * Z);

	for(int x = -1; x <= 1; x++){
		for(int y = -1; y<= 1; y++){
			for(int z = -1; z <= 1; z++){
				int cubeIndex = gridPos-x + y*Y - z*Z;
				if(cubeIndex > 0 && cubeIndex < numOfCells){
					for(int i=0; d_grid[cubeIndex+i]!= -1 && i < partsPerCell; i++){
						glm::vec3 r = positions[threadIdx.x] - positions[d_grid[cubeIndex]];
						float pressure_n = CUDART_K_F * densities[d_grid[cubeIndex]];

						glm::vec3 pressure = particleRad * (pressure_p + pressure_n) / (2 * densities[d_grid[cubeIndex]]) * grad_W_spiky(r);

						glm::vec3 viscos = particleRad * (velocities[d_grid[cubeIndex]] - velocities[threadIdx.x]) / densities[d_grid[cubeIndex]] * lap_W_viscosity(r);
						
						force += (pressure + viscos);
					}
				}
			}
		}
	}
	forces[threadIdx.x] = force;
	//printf("Density #%d = %f \n", threadIdx.x, densities[threadIdx.x]);
}
//kernel to calculate the new velocity for each particle
__global__ void calcNewVel(glm::vec3* velocities, float* densities, glm::vec3* forces, double deltaT, glm::vec3 gravity){

	glm::vec3 acceleration = (forces[threadIdx.x] / densities[threadIdx.x]) + gravity;

	//The below assignment does not work, for unknown reasons it creates NaN
	//velocities[threadIdx.x] = velocities[threadIdx.x] + acceleration * (float)deltaT;
}

//function to call all the kernels that affect positions
void cudaUpdatePos(double deltaT){

	int N = Basic.numOfParticles;

	calcDensities<<<1,N>>>(Basic.d_densities, Basic.d_positions, Basic.d_grid,  Basic.particleRadius, Basic.gridSize, Basic.cellSize, Basic.numOfCells, Basic.partsPerCell);

	calcForce<<<1,N>>>(Basic.d_forces, Basic.d_positions, Basic.d_velocities, Basic.d_densities, Basic.d_grid,  Basic.particleRadius, Basic.gridSize, Basic.cellSize, Basic.numOfCells, Basic.partsPerCell);

	calcNewVel<<<1,N>>>(Basic.d_velocities, Basic.d_densities, Basic.d_forces, deltaT, Basic.gravity);

	//clear old grid positions
	for(int i = 0; i < Basic.arraySize; i ++){
		Basic.grid[i] = -1;
	}

	int gridSize = sizeof(int) * Basic.arraySize;
	cudaMemcpy(Basic.d_grid, Basic.grid, gridSize, cudaMemcpyHostToDevice);

	updatePos<<<1,N>>>(Basic.d_positions, Basic.d_velocities, deltaT, Basic.d_grid, Basic.particleRadius, Basic.gridSize, Basic.cellSize, Basic.partsPerCell, Basic.gravity);

}


//****************6..........Draw ..............***************
//function to draw using marching cubes
void cudaDraw(Shader* myShader){
	cudaMarching(myShader);

	int numOfTris = getNumOfTriangles();
	int numOfCells = getNumOfCells();
	glm::vec3* triangles = getTriangles();
	glm::vec3* normals = getNormals();

	int iterator = 0;
	for(int i=0; i < numOfTris*3; i+=3){

		Basic.metaVerts[iterator  ] = triangles[ i ].x;	Basic.metaVerts[iterator+1] = triangles[ i ].y;	Basic.metaVerts[iterator+2] = triangles[ i ].z;
		Basic.metaVerts[iterator+3] = triangles[i+1].x;	Basic.metaVerts[iterator+4] = triangles[i+1].y;	Basic.metaVerts[iterator+5] = triangles[i+1].z;
		Basic.metaVerts[iterator+6] = triangles[i+2].x;	Basic.metaVerts[iterator+7] = triangles[i+2].y;	Basic.metaVerts[iterator+8] = triangles[i+2].z;

		Basic.metaCols[iterator  ] = 0.251;	Basic.metaCols[iterator+1] = 0.643;	Basic.metaCols[iterator+2] = 0.875;
		Basic.metaCols[iterator+3] = 0.251;	Basic.metaCols[iterator+4] = 0.643;	Basic.metaCols[iterator+5] = 0.875;
		Basic.metaCols[iterator+6] = 0.251;	Basic.metaCols[iterator+7] = 0.643;	Basic.metaCols[iterator+8] = 0.875;

		Basic.metaNorms[iterator  ] = normals[ i ].x;	Basic.metaNorms[iterator+1] = normals[ i ].y;	Basic.metaNorms[iterator+2] = normals[ i ].z;
		Basic.metaNorms[iterator+3] = normals[i+1].x;	Basic.metaNorms[iterator+4] = normals[i+1].y;	Basic.metaNorms[iterator+5] = normals[i+1].z;
		Basic.metaNorms[iterator+6] = normals[i+2].x;	Basic.metaNorms[iterator+7] = normals[i+2].y;	Basic.metaNorms[iterator+8] = normals[i+2].z;

		Basic.tris[ i ] = i;
		Basic.tris[i+1] = i+1;
		Basic.tris[i+2] = i+2;

		iterator += 9;
	}

	// VAO allocation
	glGenVertexArrays(1, &Basic.vaoID);

	// First VAO setup
	glBindVertexArray(Basic.vaoID);
	
	glGenBuffers(3, Basic.vboID);
	
	
	glBindBuffer(GL_ARRAY_BUFFER, Basic.vboID[0]);
	//initialises data storage of vertex buffer object
	glBufferData(GL_ARRAY_BUFFER, numOfTris*3*3*sizeof(GLfloat), Basic.metaVerts, GL_DYNAMIC_DRAW);
	GLint vertexLocation= glGetAttribLocation(myShader->handle(), "in_Position");
	glVertexAttribPointer(vertexLocation, 3, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(vertexLocation);

	glBindBuffer(GL_ARRAY_BUFFER, Basic.vboID[1]);
	//initialises data storage of vertex buffer object
	glBufferData(GL_ARRAY_BUFFER, numOfTris*3*3*sizeof(GLfloat), Basic.metaNorms, GL_DYNAMIC_DRAW);
	GLint normalLocation = glGetAttribLocation(myShader->handle(), "in_Normal");
	glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, 0,0);
	glEnableVertexAttribArray(normalLocation);

	
	glBindBuffer(GL_ARRAY_BUFFER, Basic.vboID[2]);
	glBufferData(GL_ARRAY_BUFFER, numOfTris*3*3*sizeof(GLfloat), Basic.metaCols, GL_DYNAMIC_DRAW);
	GLint colorLocation= glGetAttribLocation(myShader->handle(), "in_Color");
	glVertexAttribPointer(colorLocation, 3, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(colorLocation);


	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glGenBuffers(1, &Basic.metaIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Basic.metaIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numOfTris * 3 * sizeof(unsigned int), Basic.tris, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	//DRAW
	glBindVertexArray(Basic.vaoID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, Basic.cubeMapTex);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Basic.metaIBO);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, numOfTris * 3, GL_UNSIGNED_INT, 0);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0); //unbind the vertex array object
	//glDeleteBuffers(2, Basic.vboID);
	//glDeleteBuffers(1, &Basic.metaIBO);
}


//****************7....... CUBE COLLISION......****************
//kernel to collide particles with the 3 cubes that map the cliff environment
__global__ void collideCube(glm::vec3* positions, glm::vec3* velocities, glm::vec3 riverCentre, glm::vec3 riverDim, glm::vec3 cliffCentre, glm::vec3 cliffDim,
							glm::vec3 plungeCentre, glm::vec3 plungeDim, int particleRad, float Damping, int* d_grid, int gridSize) {

	glm::vec3 cen = positions[threadIdx.x];
	glm::vec3 vel = velocities[threadIdx.x];
	glm::vec3 newVel;

	if(cen.y < -75){
		//if in pool check against plunge cube
		if(cen.x + particleRad > plungeCentre.x + plungeDim.x || cen.x - particleRad < plungeCentre.x - plungeDim.x){
			newVel = glm::vec3(-(vel.x * Damping), vel.y, vel.z);
			cen = (cen + newVel);
			vel = newVel;
		}
		if(cen.y + particleRad > plungeCentre.y + plungeDim.y || cen.y - particleRad < plungeCentre.y - plungeDim.y){
			newVel = glm::vec3(vel.x, -(vel.y * Damping), vel.z);
			cen = (cen + newVel);
			vel = newVel;
		}
		if(cen.z + particleRad > plungeCentre.z + plungeDim.z || cen.z - particleRad < plungeCentre.z - plungeDim.z){
			newVel = glm::vec3(vel.x, vel.y, -(vel.z * Damping));
			cen = (cen + newVel);
			vel = newVel;
		}
	}else{
		if(cen.z < -20){
			//else if on cliff check againt cliff cube
			if(cen.x + particleRad > cliffCentre.x + cliffDim.x || cen.x - particleRad < cliffCentre.x - cliffDim.x){
			newVel = glm::vec3(-(vel.x * Damping), vel.y, vel.z);
			cen = (cen + newVel);
			vel = newVel;
			}
			if(cen.y + particleRad > cliffCentre.y + cliffDim.y || cen.y - particleRad < cliffCentre.y - cliffDim.y){
				newVel = glm::vec3(vel.x, -(vel.y * Damping), vel.z);
				cen = (cen + newVel);
				vel = newVel;
			}
			if(cen.z + particleRad > cliffCentre.z + cliffDim.z || cen.z - particleRad < cliffCentre.z - cliffDim.z){
				newVel = glm::vec3(vel.x, vel.y, -(vel.z * Damping));
				cen = (cen + newVel);
				vel = newVel;
			}
		}else{
			//else check against river cube
			if(cen.x + particleRad > riverCentre.x + riverDim.x || cen.x - particleRad < riverCentre.x - riverDim.x){
			newVel = glm::vec3(-(vel.x * Damping), vel.y, vel.z);
			cen = (cen + newVel);
			vel = newVel;
			}
			if(cen.y + particleRad > riverCentre.y + riverDim.y || cen.y - particleRad < riverCentre.y - riverDim.y){
				newVel = glm::vec3(vel.x, -(vel.y * Damping), vel.z);
				cen = (cen + newVel);
				vel = newVel;
			}
			if(cen.z + particleRad > riverCentre.z + riverDim.z || cen.z - particleRad < riverCentre.z - riverDim.z){
				newVel = glm::vec3(vel.x, vel.y, -(vel.z * Damping));
				cen = (cen + newVel);
				vel = newVel;
			}
		}
	}

	//Clear old grid pos
	int indexX = cen.x / particleRad;
	int indexY = cen.y / particleRad;
	int indexZ = cen.z / particleRad;
	int gridPos = indexX + (indexY * gridSize) + (indexZ * (gridSize * gridSize));
	d_grid[gridPos] = -1;

	positions[threadIdx.x] = cen;

	//set new grid pos
	indexX = positions[threadIdx.x].x / particleRad;
	indexY = positions[threadIdx.x].y / particleRad;
	indexZ = positions[threadIdx.x].z / particleRad;
	gridPos = indexX + (indexY * gridSize) + (indexZ * (gridSize * gridSize));
	d_grid[gridPos] = threadIdx.x;

	velocities[threadIdx.x] = vel;
}

//function that calls the kernel to collide against the cubes
void cudaCollideCube(Cube riverCube, Cube cliffCube, Cube plungeCube){
	int N = Basic.numOfParticles;

	glm::vec3 riverCen = riverCube.getCen();
	glm::vec3 riverDim = riverCube.getDim();

	glm::vec3 cliffCen = cliffCube.getCen();
	glm::vec3 cliffDim = cliffCube.getDim();

	glm::vec3 plungeCen = plungeCube.getCen();
	glm::vec3 plungeDim = plungeCube.getDim();

	collideCube<<<1,N>>>(Basic.d_positions, Basic.d_velocities, riverCen, riverDim, cliffCen, cliffDim, plungeCen, plungeDim, Basic.particleRadius, Basic.damping, Basic.d_grid, Basic.gridSize);

}


//****************8...... MARCHING CUBES......****************
//device function to interpolate vertices
__device__ glm::vec3 VertexInterp(float isolevel,glm::vec3 p1,glm::vec3 p2,float valp1,float valp2){
   double mu;
   glm::vec3 p;

   if (glm::abs(isolevel-valp1) < 0.00001)
      return(p1);
   if (glm::abs(isolevel-valp2) < 0.00001)
      return(p2);
   if (glm::abs(valp1-valp2) < 0.00001)
      return(p1);
   mu = (isolevel - valp1) / (valp2 - valp1);
   p.x = p1.x + mu * (p2.x - p1.x);
   p.y = p1.y + mu * (p2.y - p1.y);
   p.z = p1.z + mu * (p2.z - p1.z);

   return(p);
}

//kernel that calculates the influence on a vertex
__global__ void voxelCalculation(glm::vec3* cubeVerts, float* cubeVals, glm::vec3* positions, float particleRad,
								 int* d_grid, int d_gridSize, int numOfCells, int cellSize, int partsPerCell){


	int blockId = blockIdx.x + blockIdx.y * gridDim.x; 
	int threadId = blockId * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;

	float vertexInfluence = 0;
	glm::vec3 vertex = cubeVerts[threadId];

	int Y = d_gridSize/cellSize;
	int Z = Y * (d_gridSize/cellSize);

	int indexX = (vertex.x + d_gridSize/2) / cellSize;
	int indexY = (vertex.y + d_gridSize/2) / cellSize;
	int indexZ = (vertex.z + d_gridSize/2) / cellSize;
	int gridPos = indexX + (indexY * Y) + (indexZ * Z);

	float kentwrightdiv0 = (particleRad / ( pow(1.0, 1.5)));

	for(int x = -2; x <= 2; x++){
		for(int y = -2; y<= 2; y++){
			for(int z = -2; z <= 2; z++){
				int cubeIndex = gridPos-x + y*Y - z*Z;
				if(cubeIndex > 0 && cubeIndex < numOfCells){
					for(int i=0; d_grid[cubeIndex+i]!= -1 && i < partsPerCell; i++){
						glm::vec3 particleCen = positions[d_grid[cubeIndex +i]];
						//Kenwright Equation
						double divisor = (glm::length(particleCen - vertex));
						if(divisor == 0){
							vertexInfluence += kentwrightdiv0;
						}else{
							vertexInfluence += (particleRad / ( pow(divisor, 1.5)));
						}
					}
				}
			}
		}
	}
	cubeVals[threadId] = vertexInfluence;
}

//kernel that calcuklates the triangles generated from the voxel calculations
__global__ void generateTriangles( glm::vec3* triangles, int* nTriangles, glm::vec3* cubeVerts, float* cubeVals, int* d_edgeTable,
								int* d_triTable, int cubeSize, int gridSize, int cellsPerRow, int cellsPerSlice){

	//Indexing
	int index8 = (threadIdx.x + (threadIdx.y * cellsPerRow) + (blockIdx.x * cellsPerSlice)) * 8;
	

	int cubeIndex = 0;

	if(cubeVals[index8    ] < 1.0f)
		cubeIndex |= 1;
	if(cubeVals[index8 + 1] < 1.0f)
		cubeIndex |= 2;
	if(cubeVals[index8 + 2] < 1.0f)
		cubeIndex |= 4;
	if(cubeVals[index8 + 3] < 1.0f)
		cubeIndex |= 8;
	if(cubeVals[index8 + 4] < 1.0f)
		cubeIndex |= 16;
	if(cubeVals[index8 + 5] < 1.0f)
		cubeIndex |= 32;
	if(cubeVals[index8 + 6] < 1.0f)
		cubeIndex |= 64;
	if(cubeVals[index8 + 7] < 1.0f)
		cubeIndex |= 128;

	glm::vec3 vertlist [12];
	
	/* Find the vertices where the surface intersects the cube */
	if (d_edgeTable[cubeIndex] & 1){
		vertlist[0] = VertexInterp( 1.0f,cubeVerts[index8 + 0],cubeVerts[index8 + 1],cubeVals[index8 + 0],cubeVals[index8 + 1]);
	}
	if (d_edgeTable[cubeIndex] & 2){
		vertlist[1] = VertexInterp( 1.0f,cubeVerts[index8 + 1],cubeVerts[index8 + 2],cubeVals[index8 + 1],cubeVals[index8 + 2]);
	}
	if (d_edgeTable[cubeIndex] & 4){
		vertlist[2] =  VertexInterp( 1.0f,cubeVerts[index8 + 2],cubeVerts[index8 + 3],cubeVals[index8 + 2],cubeVals[index8 + 3]);
	}
	if (d_edgeTable[cubeIndex] & 8){
		vertlist[3] =  VertexInterp( 1.0f,cubeVerts[index8 + 3],cubeVerts[index8 + 0],cubeVals[index8 + 3],cubeVals[index8 + 0]);
	}
	if (d_edgeTable[cubeIndex] & 16){
		vertlist[4] = VertexInterp( 1.0f,cubeVerts[index8 + 4],cubeVerts[index8 + 5],cubeVals[index8 + 4],cubeVals[index8 + 5]);
	}
	if (d_edgeTable[cubeIndex] & 32){
		vertlist[5] = VertexInterp( 1.0f,cubeVerts[index8 + 5],cubeVerts[index8 + 6],cubeVals[index8 + 5],cubeVals[index8 + 6]);
	}
	if (d_edgeTable[cubeIndex] & 64){
		vertlist[6] = VertexInterp( 1.0f,cubeVerts[index8 + 6],cubeVerts[index8 + 7],cubeVals[index8 + 6],cubeVals[index8 + 7]);
	}
	if (d_edgeTable[cubeIndex] & 128){
		vertlist[7] = VertexInterp( 1.0f,cubeVerts[index8 + 7],cubeVerts[index8 + 4],cubeVals[index8 + 7],cubeVals[index8 + 4]);
	}
	if (d_edgeTable[cubeIndex] & 256){
		vertlist[8] = VertexInterp( 1.0f,cubeVerts[index8 + 0],cubeVerts[index8 + 4],cubeVals[index8 + 0],cubeVals[index8 + 4]);
	}
	if (d_edgeTable[cubeIndex] & 512){
		vertlist[9] = VertexInterp( 1.0f,cubeVerts[index8 + 1],cubeVerts[index8 + 5],cubeVals[index8 + 1],cubeVals[index8 + 5]);
	}
	if (d_edgeTable[cubeIndex] & 1024){
		vertlist[10] = VertexInterp( 1.0f,cubeVerts[index8 + 2],cubeVerts[index8 + 6],cubeVals[index8 + 2],cubeVals[index8 + 6]);
	}
	if (d_edgeTable[cubeIndex] & 2048){
		vertlist[11] = VertexInterp( 1.0f,cubeVerts[index8 + 3],cubeVerts[index8 + 7],cubeVals[index8 + 3],cubeVals[index8 + 7]);
	}

	/* Create the triangle */
	int cubeIndex16 = cubeIndex*16;
	int iter;
	if( d_triTable[cubeIndex16]!=-1)
		iter = atomicAdd(nTriangles, 1) * 3;

	for(int j=0; d_triTable[cubeIndex16 +j]!=-1; j+=3) {
		if(j >= 3)
			iter = atomicAdd(nTriangles, 1) * 3;
		triangles[iter   ] = vertlist[ d_triTable[cubeIndex16 +j  ] ];
		triangles[iter +1] = vertlist[ d_triTable[cubeIndex16 +(j+1)] ];
		triangles[iter +2] = vertlist[ d_triTable[cubeIndex16 +(j+2)] ];
	}
}

//function to calculate the vertex normals using he partial derivatives of the influence summation
void vertexNormals(glm::vec3* normals, glm::vec3* triangles, int numOfTris, glm::vec3* positions, float particleRad, int* d_grid, int d_gridSize, int numOfCells, int cellSize, int partsPerCell){

	for(int i=0; i < numOfTris*3; i ++){
	
		glm::vec3 normal = glm::vec3(0,0,0);

		glm::vec3 vertex = triangles[i];

		int Y = d_gridSize/cellSize;
		int Z = Y * (d_gridSize/cellSize);

		int indexX = (vertex.x + d_gridSize/2) / cellSize;
		int indexY = (vertex.y + d_gridSize/2) / cellSize;
		int indexZ = (vertex.z + d_gridSize/2) / cellSize;
		int gridPos = indexX + (indexY * Y) + (indexZ * Z);

		float kentwrightdiv0 = (particleRad / ( pow(1.0, 1.5)));

		for(int x = -2; x <= 2; x++){
			for(int y = -2; y<= 2; y++){
				for(int z = -2; z <= 2; z++){
					int cubeIndex = gridPos-x + y*Y - z*Z;
					if(cubeIndex > 0 && cubeIndex < numOfCells){
						for(int i=0; d_grid[cubeIndex+i]!= -1 && i < partsPerCell; i++){
							glm::vec3 particleCen = positions[d_grid[cubeIndex +i]];
							glm::vec3 partialDiv = glm::vec3(2*(particleCen.x - vertex.x), 2*(particleCen.y - vertex.y), 2*(particleCen.z - vertex.z));
							//Kenwright Equation with partial derivitives
							double divisor = (glm::length(particleCen - vertex));
							if(divisor == 0){
								normal += (kentwrightdiv0 * partialDiv);
							}else{
								normal += ( (float)(particleRad / ( pow(divisor, 1.5))) * partialDiv);
							}
						}
					}
				}
			}
		}
		normals[i] = glm::normalize(normal);
	}

}

//function that calls all the kernels to do with marching cubes
void cudaMarching(Shader* myShader){

	glm::vec3 startPos = glm::vec3(-Basic.gridSize/2, -Basic.gridSize/2, -Basic.gridSize/2);


	Basic.nTriangles = 0;
	cudaMemcpy(Basic.d_nTriangles, &Basic.nTriangles, sizeof(int), cudaMemcpyHostToDevice);
	
	dim3 threadsPerBlock(8, Basic.cellsPerRow);
	dim3 numOfBlocks(Basic.cellsPerRow, Basic.cellsPerRow);							

	dim3 NB_2(Basic.cellsPerRow);						// grid = cellsPerRow blocks
	dim3 TPB_2(Basic.cellsPerRow,Basic.cellsPerRow);	// block = cellsPerRow x cellsPerRow threads

	voxelCalculation<<<numOfBlocks,threadsPerBlock>>>(Basic.cubeVerts, Basic.cubeVals, Basic.d_positions, Basic.particleRadius, Basic.d_grid, Basic.gridSize, Basic.numOfCells, Basic.cellSize, Basic.partsPerCell);

	generateTriangles<<<NB_2,TPB_2>>>(Basic.d_triangles, Basic.d_nTriangles, Basic.cubeVerts, Basic.cubeVals, Basic.d_edgeTable, Basic.d_triTable, Basic.cubeSize,
									Basic.gridSize, Basic.cellsPerRow, Basic.cellsPerSlice);

	int N = getNumOfTriangles();
	int temp = ceil(N/256);
	dim3 NB_3(temp);
	dim3 TPB_3(256);

	int numOfTris = getNumOfTriangles();
	int numOfCells = getNumOfCells();
	glm::vec3* triangles = getTriangles();
	glm::vec3* normals = getNormals();

	int particleSize = sizeof(glm::vec3) * Basic.numOfParticles;
	cudaMemcpy(Basic.positions, Basic.d_positions, particleSize, cudaMemcpyDeviceToHost);

	int gridSize = sizeof(int) * Basic.arraySize;
	cudaMemcpy(Basic.grid, Basic.d_grid, gridSize, cudaMemcpyDeviceToHost);

	vertexNormals(normals, triangles, numOfTris, Basic.positions, Basic.particleRadius, Basic.grid, Basic.gridSize, Basic.numOfCells, Basic.cellSize, Basic.partsPerCell);

	int trianglesSize = (sizeof(glm::vec3) * Basic.numOfCubes * 5);
	cudaMemcpy(Basic.d_normals, Basic.normals, trianglesSize, cudaMemcpyHostToDevice);
}
