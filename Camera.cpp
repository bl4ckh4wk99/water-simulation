#include "Camera.h"
#include <iostream>
using namespace std;

Camera::Camera(){
	panAngle = 0.0f;
	zoooooom = 1000.0f;
	pos = glm::vec3(0,0,0);
	lookat = glm::vec3(0,0,0);
	up = glm::vec3(0,1,0);
	ModelMatrix = glm::mat4(1.0);
}

Camera::Camera( double deltaT, int cubeDim){
	panAngle = 0.0f;
	zoooooom = 3 * cubeDim;
	minZoom = cubeDim;
	maxZoom = 5 * cubeDim;
	pos = glm::vec3(0,0,0);
	lookat = glm::vec3(0,0,0);
	up = glm::vec3(0,1,0);
	ModelMatrix = glm::mat4(1.0);
	update(deltaT);
}

void Camera::update(double deltaT){
	ModelMatrix = glm::mat4(1.0);
	//**NORMAL CAMERA
	//Update pos with zoom and angle
	
	ModelMatrix = glm::rotate(ModelMatrix, panAngle, glm::vec3(0,1,0));

	//calc translation in direction of lookat
	glm::vec3 dir = glm::normalize(lookat);
	glm::vec3 translate = dir * zoooooom;
	ModelMatrix = glm::translate(ModelMatrix,glm::vec3(0,0,-zoooooom));

	//rotate around origin
	glm::vec3 newPos = glm::vec3(ModelMatrix * glm::vec4(0,0,0,1.0));

	pos = newPos;
	//lookat = glm::vec3(0,0,0);
	up = glm::vec3(0,1,0);
}

glm::vec3 Camera::getPos(){
	return pos;
}
glm::vec3 Camera::getLookat(){
	return lookat;
}
glm::vec3 Camera::getUp(){
	return up;
}

void Camera::panLeft(float deltaAngle){
	panAngle -= deltaAngle;
	if(panAngle < -360.0f)
		panAngle = 0.0f;
}
void Camera::panRight(float deltaAngle){
	panAngle += deltaAngle;
	if(panAngle > 360.0f)
		panAngle = 0.0f;
}

void Camera::changeLookat(float changeX, float changeY){

	lookat.x -= changeX;
	lookat.y -= changeY;

}

void Camera::zoomIn(float deltaZoom){
	zoooooom -= deltaZoom;
	if(zoooooom < minZoom)
		zoooooom = minZoom;

}

void Camera::zoomOut(float deltaZoom){
	zoooooom += deltaZoom;
	if(zoooooom > maxZoom)
		zoooooom = maxZoom;

}




