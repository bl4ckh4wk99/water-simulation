#version 330

in vec3 position;
out vec3 TexCoords;

uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;


void main()
{
	//position is as passed
    gl_Position =   ProjectionMatrix * ViewMatrix * vec4(position, 1.0);
	//tex coords are the posiiton
    TexCoords = position;
} 