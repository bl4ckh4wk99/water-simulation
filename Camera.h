#pragma once

#include "glm\glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"

class Camera{

private:
	glm::vec3 pos, lookat, up;
	float panAngle;
	float zoooooom;
	int maxZoom, minZoom;
	glm::mat4 ModelMatrix;
public:

	Camera();
	Camera(double deltaT, int cubeDim);

	//update methods
	void update(double deltaT);

	glm::vec3 getPos();
	glm::vec3 getLookat();
	glm::vec3 getUp();

	//Camera Movement functions
	void panLeft(float deltaAngle);
	void panRight(float deltaAngle);

	void changeLookat(float changeX, float changeY);

	void zoomIn(float deltaZoom);
	void zoomOut(float deltaZoom);
	

};