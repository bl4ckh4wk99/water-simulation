#pragma once
#include "../gl\glew.h"
#include "../glm\glm.hpp"
#include "../shaders/Shader.h"

const int NumberOfVertexCoords = 24;
const int NumberOfTriangleIndices = 36;

class Cube
{
private:
	//cube dimensions and origin
	glm::vec3 centre;
	float height;
	float width;
	float length;

	unsigned int m_vaoID;		    // vertex array object
	unsigned int m_vboID[2];		// two VBOs - used for colours and vertex data
	GLuint ibo;                     //identifier for the triangle indices

	//cube specifics
	static int numOfVerts;
	static int numOfTris;

	float verts[NumberOfVertexCoords];
	float cols[NumberOfVertexCoords];
	unsigned int tris[NumberOfTriangleIndices];
public:
	Cube(){}
	Cube(glm::vec3 centre, float width, float height, float length);
	void constructGeometry(Shader* myShader);
	void draw();

	glm::vec3 getCen(){return this->centre;}
	glm::vec3 getDim(){return glm::vec3(this->width,this->height,this->length);}
};