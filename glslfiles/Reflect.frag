#version 330

in vec3 ex_Normal;
in vec3 ex_EyeDir;

uniform samplerCube cubeMap;

out vec4 out_Color; //colour for the pixel

 void main(void)
 {
    vec3 reflectedDirection = normalize(reflect(ex_EyeDir, normalize(ex_Normal)));
	vec4 fragColor = texture(cubeMap, reflectedDirection);
    out_Color = fragColor;
}